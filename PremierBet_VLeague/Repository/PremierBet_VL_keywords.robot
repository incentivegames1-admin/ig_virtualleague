*** Settings ***
Library     SeleniumLibrary
#Library     XML
Library     String
Library     Process
#Library     SwingLibrary
Resource    ../Resources/PremierBet_VL_vars.robot
Resource    ../Repository/PremierBet_Common_keywords.robot
Library     DateTime


*** Keywords ***
#*********************************************** NavigatetoVirtualLeague  *************************************************
Navigate to PremierBet Virtual League
    Log to Console          Navigate to PremierBet Virtual League
    Open Browser                        ${VLURL}  ${BROWSER}
    Set Selenium Implicit Wait          10 seconds
    Maximize Browser Window
#    Title Should Be                     https://staging.betlion.ke/home/virtualleague
    Sleep       1
    Log to Console         Virtual League
    Wait Until Element Is Enabled       ${PremierBetLogoXpath}             10 seconds
    ${Date} =  Get DateTime
    Set Global Variable  ${Path}  ${EXECDIR}${/}..\\IG_VirtualLeague\\PremierBet_VLeague\\Results\\Screenshots\\PremierBet_VirtualLeague\\${Date}

#*********************************************** LoginVirtualLeague  ******************************************************
Login to PremierBet Virtual League
    Log to Console          Login to PremierBet Virtual League

    Wait Until Element Is Enabled       ${HomeLoginBtnXpath}            10 seconds
    Element Should Be Enabled           ${HomeLoginBtnXpath}
    Sleep       1

    # Click Home Login Button
    Log to Console      Click Home Login Button
    Click Element                       ${HomeLoginBtnXpath}
    Sleep       1
    Unselect Frame
    Wait Until Element Is Enabled       ${PhoneNoID}                   10 seconds

    # Login Blank Phone Number and Password
    Log to Console      Login Blank Phone Number and Password
    Click Element                       ${LoginBtnXpath}
    Sleep       1
    Page Should Contain Element         ${UsernameError}
    Page Should Contain Element         ${PasswordError}

    # Enter Phone Number Only
    Log to Console      Enter Phone Number Only
    Input Text                          ${PhoneNoIDInput}                   b2btest2
    Click Element                       ${LoginBtnXpath}
    Sleep       1
    Page Should Contain Element          ${BlankCredentialsMsg}
    Clear Element Text                  ${MobileNoID}
#
#    # Enter PIN Only
#    Log to Console      Enter PIN Only
#    Input Text                          ${PinID}                        1111
#    Click Element                       ${LoginBtnXpath}
#    Sleep       1
#    Page Should Contain Element          ${BlankCredentialsMsg}
#    Clear Element Text                  ${PinID}
#
#    # Enter Invalid Mobile Number and PIN
#    Log to Console      Enter Invalid Mobile Number and PIN
#    Input Text                          ${MobileNoID}                   881659001
#    Input Text                          ${PinID}                        9865
#
#    # Click Login Button
#    Log to Console      Click Login Button
#    Click Element                       ${LoginBtnXpath}
#    Sleep       1
#    Page Should Contain Element          ${InvalidCredentialsMsg}
#    Sleep       1
#    Clear Element Text                  ${MobileNoID}
#    Clear Element Text                  ${PinID}
#    Sleep       1
#
#    # Enter Valid Mobile Number and PIN
#    Log to Console      Enter Valid Mobile Number and PIN
#    Input Text                          ${MobileNoID}                   742343912
#    Input Text                          ${PinID}                        1111
#
#    # Click Login Button
#    Log to Console      Click Login Button
#    Click Element                       ${LoginBtnXpath}
#    Sleep       1
#
#    Wait Until Element Is Enabled       ${DepositBtnXpath}              10 seconds
#    Screenshot      BetLion_Virtual League Logged in page.png
