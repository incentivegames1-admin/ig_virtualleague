*** Variables ***
#*********************************************** LoginVirtualLeague  ******************************************************
${TYPE OF FILE}                             png
${Path}
${BROWSER}                                  chrome
${HeadlessBROWSER}                          headlesschrome
${VLURL}                                    http://premierbet-cm.btobet.net/play-game/?gameId=5340
${PremierBetLogoXpath}                      xpath://img[@class='brand-logo']
${HomeLoginBtnXpath}                        xpath://div[@class='pull-right']//div[@class='bto-user-login bto-login-sm']//button[text()='Login']
${PhoneNoID}                                xpath:(//div[@class='modal-content']//div[@class='form-group']//input[@class='form-control btosystem-enter'])[3]
${PhoneNoIDInput}                           xpath:(//div[@class='modal-content']//div[@class='form-group']//input[@class='form-control btosystem-enter error'])[1]
${PinID}                                    id:userPass
${LoginBtnXpath}                            xpath:(//button[@class='btn btn-default btn-user-confirm btn-block btosystem-login-btn'])[2]
${UsernameError}                            xpath://label[@id='username-error']
${PasswordError}                            xpath://label[@id='password-error']
${DepositBtnXpath}                          xpath://body/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/a[1]/button[1]
${WelcomePlayNowBtn}                        xpath://div[@class='virtual-league-app']//button[text()='Play Now']
${iframe}                                   xpath://iframe[@id='virtualFrame']
${BlankCredentialsMsg}                      xpath://span[text()='Please enter valid credentials']
${InvalidCredentialsMsg}                    xpath://span[text()='Sorry, that username / password combination has not been recognised Code:401']
${HamburgerMenu}                            xpath://div[@class='BLM-hamBurger-menu']
${MyAccount}                                xpath://div[@class='BLM-tabs']//a[text()='My Account']
${LogoutBtn}                                xpath://div[@class='BLM-logout-container']//button[text()='Logout']

#*********************************************** VirtualLeague  ************************************************************
${VLeagueHomeActive}                        xpath://div[@class='BLM-layout-header']//div[@class='BLM-subHeader']/ul[@class='topHeaderAddActiveClass']//li[contains(@class, 'active')]/a//i[@class='bl-icon-home']
${VLeagueHeader}                            xpath://div[@class='BLM-layout-header']//div[@class='BLM-subHeader']/ul[@class='topHeaderAddActiveClass']//li//a[@href='/Home/VirtualLeague']
${VLeagueHeaderActive}                      xpath://div[@class='BLM-layout-header']//div[@class='BLM-subHeader']/ul[@class='topHeaderAddActiveClass']//li[contains(@class, 'active')]//a[@href='/Home/VirtualLeague']
${VLeagueiframe}                            xpath://div[@class='BLM-virtual-iframe']